#An endless thread of us.

###Objective:

AETOU will be a collaborative story that allows for branching(and merging), reversion, deletion, and other features proffered by version control systems. 

Anyone can add to the trunk story, and registered users can create branches, as well as vote to delete sections from any brank or the trunk story. They will also be able to vote to merge branched sections of the story back to the trunk. Ideally, AETOU will utilize Github's API to accomodate this collaborative storywriting platform.

In addition to storywriting aspects of AETOU, users will also be able to style their own contributions using predefined themes. Registered, paying users will be able to use a subset of CSS(and maybe HTML5's canvas) to give unique styles to their story snippets. Careful consideration must be given to this, though, so as not to render snippets unviewable. Community moderation may be necessary in order to ensure that styling doesn't break the site. This might result in censorship of "ugly" or offensive styles, but that's the community's prerogative.

Progressive Enhancement will be used to ensure that users viewing the site from more capable browsers have a "beautified" experience, without sacrificing functionality, accessibility or searchability for those using less capable browsers. Ideally, all functionality will be available to all users, but it is essential that even the most incompetent browsers should be able to view the story and its branches, as well as contribute a snippet.

##Architecture

###Models

* **Users**
-- Users will need to register for an account if they wish to branch a story, or if they want to vote on any merge, revert or delete functions. Users without an account will still be able to contribute snippets, which will technically belong to a "guest" user. Registered users will build reputation based on number of contributions, length of contributions, the lifespan of these contributions, and moderation activity.

* **Snippets**
-- Snippets are *any* story element. They can be as short as a single character, and as long as the user sees fit(within the technical limitations, anyway). A snippet has one style and may belong to a user. Snippets *cannot* be directly modified, even by its owner. It can only be branched(by anyone), after which point the community can vote to merge the branch into the trunk. In this way, the community is responsible for the integrity of the story and its parts, rather than any single user. Snippets belonging to registered users will have more "resistance" to changes from the community, in that more votes are required to overwrite bits of code, and this vote count will scale according to the user's reputation.

* **Styles**
-- A style belongs to at least one snippet, and can be modified by its owner(and *only* its owner) at any point. Styles will "compile" to raw CSS, but for usability and security purposes, raw CSS may not be input by *any* user. There will be a library of "themes"(contributed by the community), but registered users with sufficient reputation will be able to tweak colors and typography to suit their snippets. The community may flag styles as inappropriate, and with enough flags, the style will be reverted to stock and the owner will be notified(penalties may be incurred for repeat offenders).